package com.fiap.view;

import org.springframework.data.repository.CrudRepository;

public interface ViewRepository extends CrudRepository<View, Integer> {

    
}